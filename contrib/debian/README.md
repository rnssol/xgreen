
Debian
====================
This directory contains files used to package xgreend/xgreen-qt
for Debian-based Linux systems. If you compile xgreend/xgreen-qt yourself, there are some useful files here.

## xgreen: URI support ##


xgreen-qt.desktop  (Gnome / Open Desktop)
To install:

	sudo desktop-file-install xgreen-qt.desktop
	sudo update-desktop-database

If you build yourself, you will either need to modify the paths in
the .desktop file or copy or symlink your xgreen-qt binary to `/usr/bin`
and the `../../share/pixmaps/xgreen128.png` to `/usr/share/pixmaps`

xgreen-qt.protocol (KDE)

