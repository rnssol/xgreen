Sample configuration files for:

SystemD: xgreend.service
Upstart: xgreend.conf
OpenRC:  xgreend.openrc
         xgreend.openrcconf
CentOS:  xgreend.init
OS X:    org.xgreen.xgreend.plist

have been made available to assist packagers in creating node packages here.

See doc/init.md for more information.
