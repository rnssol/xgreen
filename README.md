Xgreen Core staging tree 0.13.0
===============================

`master:` [![Build Status](https://travis-ci.org/xgreenpay/xgreen.svg?branch=master)](https://travis-ci.org/xgreenpay/xgreen) `develop:` [![Build Status](https://travis-ci.org/xgreenpay/xgreen.svg?branch=develop)](https://travis-ci.org/xgreenpay/xgreen/branches)

https://www.xgreen.org


What is Xgreen?
----------------

Xgreen is an experimental digital currency that enables anonymous, instant
payments to anyone, anywhere in the world. Xgreen uses peer-to-peer technology
to operate with no central authority: managing transactions and issuing money
are carried out collectively by the network. Xgreen Core is the name of the open
source software which enables the use of this currency.

For more information, as well as an immediately useable, binary version of
the Xgreen Core software, see https://www.xgreen.org/get-xgreen/.




Development Process
-------------------

The `master` branch is meant to be stable. Development is normally done in separate branches.
[Tags](https://github.com/xgreenpay/xgreen/tags) are created to indicate new official,
stable release versions of Xgreen Core.

The contribution workflow is described in [CONTRIBUTING.md](CONTRIBUTING.md).

Testing
-------

Testing and code review is the bottleneck for development; we get more pull
requests than we can review and test on short notice. Please be patient and help out by testing
other people's pull requests, and remember this is a security-critical project where any mistake might cost people
lots of money.

### Automated Testing

Developers are strongly encouraged to write [unit tests](src/test/README.md) for new code, and to
submit new unit tests for old code. Unit tests can be compiled and run
(assuming they weren't disabled in configure) with: `make check`. Further details on running
and extending unit tests can be found in [/src/test/README.md](/src/test/README.md).

There are also [regression and integration tests](/qa) of the RPC interface, written
in Python, that are run automatically on the build server.
These tests can be run (if the [test dependencies](/qa) are installed) with: `qa/pull-tester/rpc-tests.py`

The Travis CI system makes sure that every pull request is built for Windows, Linux, and OS X, and that unit/sanity tests are run automatically.

### Manual Quality Assurance (QA) Testing

Changes should be tested by somebody other than the developer who wrote the
code. This is especially important for large or high-risk changes. It is useful
to add a test plan to the pull request description if testing the changes is
not straightforward.

Translations
------------

Changes to translations as well as new translations can be submitted to
[Xgreen Core's Transifex page](https://www.transifex.com/projects/p/xgreen/).

Translations are periodically pulled from Transifex and merged into the git repository. See the
[translation process](doc/translation_process.md) for details on how this works.

**Important**: We do not accept translation changes as GitHub pull requests because the next
pull from Transifex would automatically overwrite them again.

Translators should also follow the [forum](https://www.xgreen.org/forum/topic/xgreen-worldwide-collaboration.88/).



Compiling process
-------------------

```
apt-get update

apt-get install -y curl build-essential libtool 
autotools-dev automake pkg-config python3 
bsdmainutils cmake

git clone https://[Account name here]@bitbucket.org/faisal-naveed-akbar/xgreen.git

cd dash/depends

make -j8 NO_QT=1

cd ..

./autogen.sh

./configure --prefix=$(pwd)/depends/x86_64-pc-linux-gnu

make -j8

```

# Tutorial

```
Reference vedio link

https://www.youtube.com/watch?v=mDGxGYvkDEE&list=PLHWfvuRy-9gdCv2oD_Ywd0swNF3N8mXt-&index=1
```


## Step 1

    apt-get update

apt-get install -y curl build-essential libtool autotools-dev automake pkg-config python3 bsdmainutils cmake

git clone https://github.com/dashpay/dash

## Step 2 

find . -type f -print0 | xargs -0 sed -i 's/dash/xgreen/g'

find . -type f -print0 | xargs -0 sed -i 's/Dash/Xgreen/g'

find . -type f -print0 | xargs -0 sed -i 's/DASH/XGREEN/g'


## Step 3 Renaming files and directories

```
find . -type f -name '*dash*' | while read FILE ; do

    newfile="$(echo ${FILE} |sed -e 's/dash/xgreen/g')" ;

    mv "${FILE}" "${newfile}" ;
done 

```
mv dash-docs xgreen-docs

mv doc/release-notes/dash doc/release-notes/xgreen

find -name "dash"

## Step 4 Compiling code 
```
cd dash/depends

make -j4

cd ..
```
### Alternative without GUI 
```
// changing the number of cores in make file e.g -j[CORES] and NO_QT
cd dash/depends

make -j4 NO_QT=1

cd ..

```

## Step 5 check no remaing file of dash 
```
find -name "*dash*" 
```

## Step 6 Build code 
```
./autogen.sh

./configure --prefix=$(pwd)/depends/x86_64-pc-linux-gnu

make -j4
```

## Step 7  Arguments configration

```
openssl ecparam -genkey -name secp256k1 -out alertkey.pem
openssl ec -in alertkey.pem -text > alertkey.hex

openssl ecparam -genkey -name secp256k1 -out testnetalert.pem
openssl ec -in testnetalert.pem -text > testnetalert.hex

openssl ecparam -genkey -name secp256k1 -out genesiscoinbase.pem
openssl ec -in testnetalert.pem -text > genesiscoinbase.hex

cat alertkey.hex
cat testnetalert.hex
cat genesiscoinbase.hex

```
## Step 8 	 Changing network and other parameters

1. Replace the value of genesisOutputScript in chainparams.cpp Line#81 with cat genesiscoinbase.hex public key

2. Randomly change the last bit of pchMessageStart for Testnet and Mainnet at chainparams.cpp Line#250-253 & 399-402

3. Replace the ASCII of PUBKEY_ADDRESS & PUBKEY_ADDRESS_TEST in the gen_base58_test_vectors.py in Line#21 & 23 to ASCCI of coin's first letter

4. Replace pnSeed6_main & pnSeed6_test IP's in chainparamsseeds.h to 0x0

### Block Reward: 6 coin ( 33%pos, 66%mn)

   Replace nsubsidy in validation.cpp Line#1181 which is minning block reward


### Block Target: 60 seconds

    Replace nPowTargetSpacing to 1* 60 in chainparams.cpp Line#202 & 355 which is ideal block mining time

### max supply: 50,000,000

    Replace max_money in amount.h Line#33 which is Max Supply to 50000000

### Block Maturity: 50 (30 min)

1. Replace coinbase_maturity in consensus.h Line#24 which is Block Maturity (After how many block miner can use his reward)

2. Replace the return value of AllowFreeThreshold() in txmempool.h Line#39 which is the difficulty ramp up time

### Masternode Collateral: 20,000 coin

   Replace the value 1000 in deterministicmns.cpp Line#577 & providertx.cpp Line#141 & masternode.cpp Line#131 which is Masternode Collateral

### Transaction Fee: 0.001

   Replace the value DEFAULT_TRANSACTION_MAXFEE in validation.h Line#61 which is Transaction Fee

### Confirmation: 12

    Replace RecommendedNumConfirmations in transactionrecord.h at Line#95 which is no of confirmations


## Step 9 Setting PoS Stake Block Reward: 2 coin (1/3)Masternode Block Reward: 4 coin(2/3)

In validation.cpp cahnge the value of nSubsidy on line# 1181 in function GetBlockSubsidy()

1. set ret value in GetMasternodePayment to blockValue*0.666666; // 66.33% reward to master

2. set   ret += blockValue * 0.333333;


3. Change value of ret in  GetMasternodePayment() according to choice for PoS stake % reward

4. set value of nMasternodePaymentsIncreaseBlock to 0

### Min stake age: 3 hours

set value of nMasternodePaymentsIncreasePeriod in this case it is 180 equal 3 hours



## Step 10 Genesis Block configration

```
setup Genesis Block configration 
https://github.com/lhartikk/GenesisH0#examples
```
### configure GenesisHO tool
1. git clone https://github.com/lhartikk/GenesisH0.git
2. git clone https://github.com/lhartikk/xcoin-hash.git
3. cd xcoin-hash
3. sudo python setup.py install
4. cd ..
4. cd GenesisH0
5. sudo pip install scrypt construct==2.5.2
4. python genesis.py -a X11 
-z [the pszTimestamp found in the coinbase of the genesisblock] 
-p [the pubkey found in the output script] 
-n [the first value of the nonce that will be incremented] 
-t [TIME  the (unix) time when the genesisblock is created] 
-v [ the value in coins for the output, full value (exp. in bitcoin 5000000000 - To get other coins value: Block Value * 100000000)]

5.  Sample 

```
python genesis.py -a X11 
-z "Wired 09/Dec/2019 The Grand Experiment Goes Live: Overstock.com Is Now Accepting Xgreen" 
-p "04ae5973dedcd3f6fb6636b5cbf2451c297994512b166fa0f9f033ec3b2dbb06d8f80cb350195653e0db9dab03a9f2e1d4d5aa84396e0893dab1337fa5426872cf" 
-n 29370672 
-t 1558511744 
-v 600000000
```
### Replacing genesis block values 

1. Get current value by typing command in terminal 
    ``` 
    date +%s
    ```

2. Change nTime  value in  CreateGenesisBlock to present unix time stamp of testnet and mainnet chainparams.cpp

Change genesisReward  value in  CreateGenesisBlock to present 6 * COIN of testnet and mainnet chainparams.cpp


3. Replace the value of pszTimestamp of the genesis block at line 80 in chainparams.cpp

4. eplace  hashMerkleRoot at line#268 in chainparams.cpp to the data of genesis block markleroot value