#!/bin/bash
# use testnet settings,  if you need mainnet,  use ~/.xgreencore/xgreend.pid file instead
xgreen_pid=$(<~/.xgreencore/testnet3/xgreend.pid)
sudo gdb -batch -ex "source debug.gdb" xgreend ${xgreen_pid}
