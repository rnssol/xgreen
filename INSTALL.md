Building Xgreen
=============

See doc/build-*.md for instructions on building the various
elements of the Xgreen Core reference implementation of Xgreen.
